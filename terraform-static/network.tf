resource "google_compute_network" "mainnet" {
  name = "main-net"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "mainnet" {
  name = "main-net"
  network = google_compute_network.mainnet.self_link
  ip_cidr_range = var.subnet
}

data "google_compute_network" "developer" {
  project = "developer-222401"
  name = "default"
}

resource "google_compute_network_peering" "mainnet_developer" {
  name = "mainnet-developer"
  network = google_compute_network.mainnet.self_link
  peer_network = data.google_compute_network.developer.self_link
}

resource "google_compute_network_peering" "developer_mainnet" {
  name = "developer-mainnet"
  network = data.google_compute_network.developer.self_link
  peer_network = google_compute_network.mainnet.self_link
}

resource "google_compute_firewall" "fw_public_node" {
  name = "${var.resources_name}-node-public"
  network = google_compute_network.mainnet.self_link
  priority = 530
  target_tags = ["${var.resources_name}-node"]
  allow {
    protocol = "tcp"
    ports = [22, 40403, 18080]
  }
}

resource "google_compute_firewall" "fw_public_node_rpc" {
  name = "${var.resources_name}-node-rpc"
  network = google_compute_network.mainnet.self_link
  priority = 540
  target_tags = ["${var.resources_name}-node"]
  allow {
    protocol = "tcp"
    ports = [40401]
  }
}

resource "google_compute_firewall" "fw_node_p2p" {
  name = "${var.resources_name}-node-p2p"
  network = google_compute_network.mainnet.self_link
  priority = 550
  source_ranges = ["0.0.0.0/0"]
  target_tags = ["${var.resources_name}-node"]
  allow {
    protocol = "tcp"
    ports = [40400, 40404]
  }
}

resource "google_compute_firewall" "fw_node_deny" {
  name = "${var.resources_name}-node-deny"
  network = google_compute_network.mainnet.self_link
  priority = 5010
  target_tags = ["${var.resources_name}-node"]
  deny {
    protocol = "tcp"
  }
  deny {
    protocol = "udp"
  }
}

resource "google_compute_address" "node_ext_addr" {
  count = var.node_count
  name = "${var.resources_name}-node${count.index}-ext"
  address_type = "EXTERNAL"
}

resource google_compute_address "node_int_addr" {
  count = var.node_count
  name = "${var.resources_name}-node${count.index}-int"
  address_type = "INTERNAL"
  subnetwork = google_compute_subnetwork.mainnet.self_link
  address = cidrhost(google_compute_subnetwork.mainnet.ip_cidr_range, count.index + 10)
}

resource "google_dns_record_set" "node_dns_record" {
  count = var.node_count
  name = "node${count.index}${var.dns_suffix}."
  managed_zone = "main-net"
  type = "A"
  ttl = 3600
  rrdatas = [google_compute_address.node_ext_addr[count.index].address]
}

