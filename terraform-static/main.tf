variable "resources_name" { default = "root-shard" }
variable "gcp_zone" { default = "europe-west1-b" }
variable "node_count" { default = 9 }
variable "dns_suffix" { default = ".root-shard.mainnet.rchain.coop" }
variable "subnet"     { default = "10.8.0.0/26" }

provider "google" {
  project = "main-net"
  zone = var.gcp_zone
}

provider "google-beta" {
  project = "main-net"
  zone = var.gcp_zone
}

terraform {
  required_version = ">= 0.12"
  backend "gcs" {
    bucket = "rchain-terraform-state"
    prefix = "main-net"
  }
}
