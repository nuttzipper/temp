data "google_compute_subnetwork" "mainnet" {
  name = "main-net"
}

data google_compute_address "node_addr_ext" {
  count = var.node_count
  name = "${var.resources_name}-node${count.index}-ext"
}

data google_compute_address "node_addr_int" {
  count = var.node_count
  name = "${var.resources_name}-node${count.index}-int"
}

resource "google_compute_instance" "node_host" {
  count = var.node_count
  name = "${var.resources_name}-node${count.index}"
  hostname = "node${count.index}${var.dns_suffix}"
  machine_type = "n1-standard-8"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
      size = 500
      type = "pd-ssd"
    }
  }

  tags = [
    "${var.resources_name}-node",
    "collectd-out",
    "elasticsearch-out",
    "logstash-tcp-out",
    "logspout-http",
  ]

  network_interface {
    subnetwork = data.google_compute_subnetwork.mainnet.self_link
    network_ip = data.google_compute_address.node_addr_int[count.index].address
    access_config {
      nat_ip = data.google_compute_address.node_addr_ext[count.index].address
    }
  }

  connection {
    type = "ssh"
    host = self.network_interface[0].access_config[0].nat_ip
    user = "root"
    private_key = file("~/.ssh/google_compute_engine")
  }

  provisioner "file" {
    source = var.rshard-secret-key
    destination = "/root/rshard-git-crypt-secret.key"
  }

  provisioner "file" {
    source = var.rshard-git-deploy-key
    destination = "/root/rshard-git-deploy-key"
  }

  provisioner "remote-exec" {
    script = "../bootstrap"
  }
}

